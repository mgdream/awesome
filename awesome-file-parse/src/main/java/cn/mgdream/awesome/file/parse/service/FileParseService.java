package cn.mgdream.awesome.file.parse.service;

import cn.mgdream.awesome.file.parse.domain.Parsed;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.stream.Stream;

public interface FileParseService {

    Stream<Parsed> parse(MultipartFile file) throws IOException;
}
