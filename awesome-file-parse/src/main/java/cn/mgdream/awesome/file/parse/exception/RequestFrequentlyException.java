package cn.mgdream.awesome.file.parse.exception;

public class RequestFrequentlyException extends RuntimeException {

    public RequestFrequentlyException() {
        super();
    }

    public RequestFrequentlyException(String message) {
        super(message);
    }

    public RequestFrequentlyException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestFrequentlyException(Throwable cause) {
        super(cause);
    }

    protected RequestFrequentlyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
