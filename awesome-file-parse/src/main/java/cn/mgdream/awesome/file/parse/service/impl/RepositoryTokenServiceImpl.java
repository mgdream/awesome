package cn.mgdream.awesome.file.parse.service.impl;

import cn.mgdream.awesome.file.parse.repository.TokenRepository;
import cn.mgdream.awesome.file.parse.service.TokenService;
import org.springframework.stereotype.Service;

@Service
public class RepositoryTokenServiceImpl implements TokenService {

    private final TokenRepository tokenRepository;

    public RepositoryTokenServiceImpl(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public String newToken() {
        return tokenRepository.newToken();
    }

    @Override
    public boolean contains(String token) {
        return tokenRepository.contains(token);
    }
}
