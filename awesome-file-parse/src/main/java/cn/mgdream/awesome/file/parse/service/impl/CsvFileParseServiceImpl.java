package cn.mgdream.awesome.file.parse.service.impl;

import cn.mgdream.awesome.file.parse.domain.Parsed;
import cn.mgdream.awesome.file.parse.service.FileParseService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
public class CsvFileParseServiceImpl implements FileParseService {

    @Override
    public Stream<Parsed> parse(MultipartFile file) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(file.getInputStream());
        Iterable<CSVRecord> csvRecords = CSVFormat.DEFAULT.parse(inputStreamReader);
        return StreamSupport
                .stream(csvRecords.spliterator(), false)
                .map(csvRecord -> {
                    Parsed parsed = new Parsed();
                    parsed.setColumn0(csvRecord.get(0));
                    parsed.setColumn1(csvRecord.get(1));
                    parsed.setColumn2(csvRecord.get(2));
                    return parsed;
                });
    }
}
