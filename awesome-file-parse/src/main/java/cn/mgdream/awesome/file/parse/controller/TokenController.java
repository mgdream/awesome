package cn.mgdream.awesome.file.parse.controller;

import cn.mgdream.awesome.file.parse.service.TokenService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController {

    private final TokenService tokenService;

    public TokenController(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @GetMapping("/token")
    public String newToken() {
        return tokenService.newToken();
    }
}
