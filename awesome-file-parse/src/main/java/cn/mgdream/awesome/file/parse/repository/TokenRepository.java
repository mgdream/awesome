package cn.mgdream.awesome.file.parse.repository;

public interface TokenRepository {

    String newToken();

    boolean contains(String token);
}
