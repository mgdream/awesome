package cn.mgdream.awesome.file.parse.controller;

import cn.mgdream.awesome.file.parse.domain.Parsed;
import cn.mgdream.awesome.file.parse.exception.RequestFrequentlyException;
import cn.mgdream.awesome.file.parse.service.FileParseService;
import cn.mgdream.awesome.file.parse.service.ParsedService;
import cn.mgdream.awesome.file.parse.service.TokenService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FileParseController {

    private final FileParseService fileParseService;

    private final ParsedService parsedService;

    private final TokenService tokenService;

    public FileParseController(FileParseService fileParseService, ParsedService parsedService, TokenService tokenService) {
        this.fileParseService = fileParseService;
        this.parsedService = parsedService;
        this.tokenService = tokenService;
    }

    @PostMapping("/parse")
    public List<Parsed> parse(MultipartFile file, String token) throws IOException {
        if (tokenService.contains(token)) {
            throw new RequestFrequentlyException("Upload file but not refresh request token!");
        }
        List<Parsed> parsedList = fileParseService.parse(file).collect(Collectors.toList());
        parsedService.save(parsedList.stream());
        return parsedList;
    }
}
