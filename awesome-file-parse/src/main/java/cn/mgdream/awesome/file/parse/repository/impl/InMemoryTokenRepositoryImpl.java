package cn.mgdream.awesome.file.parse.repository.impl;

import cn.mgdream.awesome.file.parse.repository.TokenRepository;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class InMemoryTokenRepositoryImpl implements TokenRepository {

    private static final int REPOSITORY_INITIAL_CAPACITY = 0x10;

    private static final long TOKEN_EXPIRED_TIMEOUT = 60000;

    private static final Map<String, Long> REPOSITORY = new ConcurrentHashMap<>(REPOSITORY_INITIAL_CAPACITY);

    @Override
    public String newToken() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    @Override
    public boolean contains(String token) {
        Long lastTimestamp = REPOSITORY.get(token);
        long currentTimestamp = System.currentTimeMillis();
        boolean contains =
                Objects.nonNull(lastTimestamp)
                        && (lastTimestamp + TOKEN_EXPIRED_TIMEOUT > currentTimestamp);
        if (!contains) {
            REPOSITORY.put(token, currentTimestamp);
        }
        return contains;
    }
}
