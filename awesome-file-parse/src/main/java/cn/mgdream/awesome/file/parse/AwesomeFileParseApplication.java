package cn.mgdream.awesome.file.parse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwesomeFileParseApplication {

    public static void main(String[] args) {
        SpringApplication.run(AwesomeFileParseApplication.class, args);
    }

}
