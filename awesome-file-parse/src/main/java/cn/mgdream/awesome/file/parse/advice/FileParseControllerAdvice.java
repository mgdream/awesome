package cn.mgdream.awesome.file.parse.advice;

import cn.mgdream.awesome.file.parse.controller.FileParseController;
import cn.mgdream.awesome.file.parse.domain.ErrorDetails;
import cn.mgdream.awesome.file.parse.exception.RequestFrequentlyException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@RestControllerAdvice(assignableTypes = FileParseController.class)
public class FileParseControllerAdvice {

    @ExceptionHandler
    public HttpEntity requestFrequentlyExceptionHandler(RequestFrequentlyException e, HttpServletRequest request) {
        ErrorDetails errorDetails = new ErrorDetails();
        errorDetails.setTimestamp(LocalDateTime.now());
        errorDetails.setStatus(HttpStatus.TOO_EARLY.value());
        errorDetails.setError(HttpStatus.TOO_EARLY.getReasonPhrase());
        errorDetails.setMessage(e.getLocalizedMessage());
        errorDetails.setPath(request.getServletPath());
        return ResponseEntity
                .status(HttpStatus.TOO_EARLY)
                .body(errorDetails);
    }
}
