package cn.mgdream.awesome.file.parse.service.impl;

import cn.mgdream.awesome.file.parse.domain.Parsed;
import cn.mgdream.awesome.file.parse.repository.ParsedRepository;
import cn.mgdream.awesome.file.parse.service.ParsedService;
import org.springframework.stereotype.Service;

import java.util.stream.Stream;

@Service
public class RepositoryParsedServiceImpl implements ParsedService {

    private final ParsedRepository parsedRepository;

    public RepositoryParsedServiceImpl(ParsedRepository parsedRepository) {
        this.parsedRepository = parsedRepository;
    }

    @Override
    public void save(Stream<Parsed> parsedStream) {
        parsedStream.forEach(parsedRepository::save);
    }
}
