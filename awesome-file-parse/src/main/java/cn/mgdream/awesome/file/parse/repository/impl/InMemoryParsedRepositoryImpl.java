package cn.mgdream.awesome.file.parse.repository.impl;

import cn.mgdream.awesome.file.parse.domain.Parsed;
import cn.mgdream.awesome.file.parse.repository.ParsedRepository;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class InMemoryParsedRepositoryImpl implements ParsedRepository {

    private static final int DEFAULT_REPOSITORY_CAPACITY = 0x10;

    private static final long DEFAULT_INITIAL_ID = 0L;

    private static final Map<Long, Parsed> REPOSITORY = new ConcurrentHashMap<>(DEFAULT_REPOSITORY_CAPACITY);

    private static final AtomicLong ID = new AtomicLong(DEFAULT_INITIAL_ID);

    @Override
    public void save(Parsed parsed) {
        if (parsed.getId() == null) {
            parsed.setId(ID.getAndIncrement());
        }
        REPOSITORY.put(parsed.getId(), parsed);
    }
}
