package cn.mgdream.awesome.file.parse.repository;

import cn.mgdream.awesome.file.parse.domain.Parsed;

public interface ParsedRepository {

    void save(Parsed parsed);
}
