package cn.mgdream.awesome.file.storage.controller;

import cn.mgdream.awesome.file.storage.service.FileStorageService;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/")
public class FileStorageController {

    private final FileStorageService fileStorageService;

    public FileStorageController(FileStorageService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }

    @PostMapping
    public HttpEntity post(MultipartFile file) throws IOException {
        fileStorageService.store(file);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(null);
    }

    @GetMapping
    public HttpEntity get() throws IOException {
        List<String> filenames =
                fileStorageService
                        .loadAll()
                        .map(Path::getFileName)
                        .map(Path::toString)
                        .collect(Collectors.toList());
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(filenames);
    }

    @GetMapping("/{filename:.+}")
    public HttpEntity get(@PathVariable String filename) throws MalformedURLException, FileNotFoundException {
        Path path = fileStorageService.load(filename);
        Resource resource = new UrlResource(path.toUri());
        return ResponseEntity
                .status(HttpStatus.OK)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
                .body(resource);
    }

    @PutMapping
    public HttpEntity put(MultipartFile file) throws IOException {
        fileStorageService.update(file);
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .body(null);
    }

    @DeleteMapping("/{filename:.+}")
    public HttpEntity delete(@PathVariable String filename) throws IOException {
        fileStorageService.delete(filename);
        return ResponseEntity
                .status(HttpStatus.NO_CONTENT)
                .body(null);
    }
}
