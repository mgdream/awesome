package cn.mgdream.awesome.file.storage;

import cn.mgdream.awesome.file.storage.properties.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(FileStorageProperties.class)
public class AwesomeFileStorageApplication {

    public static void main(String[] args) {
        SpringApplication.run(AwesomeFileStorageApplication.class, args);
    }

}
