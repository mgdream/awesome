package cn.mgdream.awesome.file.storage.service.impl;

import cn.mgdream.awesome.file.storage.properties.FileStorageProperties;
import cn.mgdream.awesome.file.storage.service.FileStorageService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.stream.Stream;

@Service
public class FileSystemFileStorageServiceImpl implements FileStorageService {

    private final Path location;

    public FileSystemFileStorageServiceImpl(FileStorageProperties fileStorageProperties) throws IOException {
        this.location = Paths.get(fileStorageProperties.getLocation());
        if (Files.notExists(this.location)) {
            Files.createDirectories(this.location);
        }
    }

    @Override
    public void store(MultipartFile file) throws IOException {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try (InputStream inputStream = file.getInputStream()) {
            Files.copy(inputStream, location.resolve(filename));
        }
    }

    @Override
    public Stream<Path> loadAll() throws IOException {
        return Files.walk(location, 1)
                .filter(path -> !Objects.equals(location, path))
                .map(location::relativize);
    }

    @Override
    public Path load(String filename) throws FileNotFoundException {
        Path filePath = location.resolve(filename);
        if (Files.notExists(filePath)) {
            throw new FileNotFoundException("Cannot load file! File " + filename + " not exists!");
        }
        return filePath;
    }

    @Override
    public void update(MultipartFile file) throws IOException {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try (InputStream inputStream = file.getInputStream()) {
            Path filePath = location.resolve(filename);
            if (Files.notExists(filePath)) {
                throw new FileNotFoundException("Cannot update file! File " + filename + " not exists!");
            }
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    @Override
    public void delete(String filename) throws IOException {
        Path filePath = location.resolve(filename);
        if (Files.notExists(filePath)) {
            throw new FileNotFoundException("Cannot delete file! File " + filename + " not exists!");
        }
        Files.delete(filePath);
    }
}
