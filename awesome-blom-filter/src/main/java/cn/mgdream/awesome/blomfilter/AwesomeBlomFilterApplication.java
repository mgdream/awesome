package cn.mgdream.awesome.blomfilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwesomeBlomFilterApplication {

    public static void main(String[] args) {
        SpringApplication.run(AwesomeBlomFilterApplication.class, args);
    }

}
