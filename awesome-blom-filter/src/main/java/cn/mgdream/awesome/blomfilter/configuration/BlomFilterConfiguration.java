package cn.mgdream.awesome.blomfilter.configuration;

import cn.mgdream.awesome.blomfilter.core.BlomFilter;
import cn.mgdream.awesome.blomfilter.core.BlomFilterFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
public class BlomFilterConfiguration {

    private static final String DEFAULT_BLOM_FILTER_KEY = "blom_filter";

    @Bean
    public BlomFilter redisBlomFilter(RedisTemplate<String, String> redisTemplate) {
        return BlomFilterFactory.redisBlomFilter(DEFAULT_BLOM_FILTER_KEY, redisTemplate);
    }
}
