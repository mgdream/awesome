package cn.mgdream.awesome.blomfilter.core;

import org.springframework.data.redis.core.RedisTemplate;

public class BlomFilterFactory {

    public static BlomFilter redisBlomFilter(String key, RedisTemplate<String, String> redisTemplate) {
        return new RedisBlomFilter(key, redisTemplate);
    }
}
