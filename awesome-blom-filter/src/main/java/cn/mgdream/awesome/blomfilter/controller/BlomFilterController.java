package cn.mgdream.awesome.blomfilter.controller;

import cn.mgdream.awesome.blomfilter.core.BlomFilter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@RestController
public class BlomFilterController {

    private final BlomFilter blomFilter;

    private final DataSource dataSource;

    public BlomFilterController(BlomFilter blomFilter, DataSource dataSource) {
        this.blomFilter = blomFilter;
        this.dataSource = dataSource;
    }

    @GetMapping("/{id}")
    public boolean get(@PathVariable String id) throws SQLException {
        if (blomFilter.possibleContains(id)) {
            return containsId(id);
        }
        return false;
    }

    @PutMapping("/{id}")
    public void put(@PathVariable String id) throws SQLException {
        putId(id);
        blomFilter.add(id);
    }

    private boolean containsId(String id) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(1) FROM `demo` WHERE `id` = ?");
        preparedStatement.setString(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        boolean contains = false;
        while (resultSet.next()) {
            contains = resultSet.getBoolean(1);
        }
        return contains;
    }

    private void putId(String id) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT IGNORE INTO `demo` (`id`) VALUE (?)");
        preparedStatement.setString(1, id);
        preparedStatement.execute();
    }
}
