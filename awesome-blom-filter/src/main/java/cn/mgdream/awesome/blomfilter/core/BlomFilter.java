package cn.mgdream.awesome.blomfilter.core;

public interface BlomFilter {

    <T> void add(T value);

    <T> boolean possibleContains(T value);
}
