package cn.mgdream.awesome.blomfilter.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;

class RedisBlomFilter implements BlomFilter {

    private static final Logger logger = LoggerFactory.getLogger(RedisBlomFilter.class);

    private final String key;

    private final RedisTemplate<String, String> redisTemplate;

    public RedisBlomFilter(String key, RedisTemplate<String, String> redisTemplate) {
        this.key = key;
        this.redisTemplate = redisTemplate;
    }

    public <T> void add(T value) {
        redisTemplate.opsForValue().setBit(key, value.hashCode(), true);
    }

    public <T> boolean possibleContains(T value) {
        Boolean possibleContains = redisTemplate.opsForValue().getBit(key, value.hashCode());
        if (possibleContains == null) {
            throw new IllegalStateException();
        }
        if (possibleContains) {
            logger.info("Key[{}] possible contains {}", key, value);
        } else {
            logger.info("Key[{}] impossible contains {}", key, value);
        }
        return possibleContains;
    }
}
