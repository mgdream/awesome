package cn.mgdream.awesome.verify.code.service;

public interface SendMessageService {

    void send(String key, String message);
}
