package cn.mgdream.awesome.verify.code;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwesomeVerifyCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(AwesomeVerifyCodeApplication.class, args);
    }

}
