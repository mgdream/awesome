package cn.mgdream.awesome.verify.code.service;

import java.awt.*;

public interface GenerateImageService {

    Image imageWithDisturb(String string);
}
