package cn.mgdream.awesome.verify.code.repository;

public interface VerifyCodeRepository {

    void save(String key, String code);

    String find(String key);
}
